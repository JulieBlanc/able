21/03/2017 - Julie Blanc  

# État de l'existant des modules éditoriaux


## Modules interactifs : librairies graphiques et interactives

* **Mobilizing** ([https://www.mobilizing-js.net/fr](https://www.mobilizing-js.net/fr))

* **p5 js** ([https://p5js.org/](https://p5js.org/))  
JS client-side library for creating graphic and interactive experiences, based on the core principles of Processing.

* **D3** ([https://d3js.org/](https://p5js.org/))  
JavaScript library for manipulating documents based on data.

* **Idyll** [(https://idyll-lang.github.io/introduction](https://idyll-lang.github.io/introduction))  
Idyll is a tool that makes it easier to author interactive narratives for the web. The goal of the project is to provide a friendly markup language — and an associated toolchain — that can be used to create dynamic, text-driven web page

* **vis js** ([http://visjs.org/](http://visjs.org/))  
A dynamic, browser based visualization library. The library is designed to be easy to use, to handle large amounts of dynamic data, and to enable manipulation of and interaction with the data. The library consists of the components DataSet, Timeline, Network, Graph2d and Graph3d.

* **three.js** ([https://threejs.org/](https://threejs.org/))  
bibliothèque JavaScript pour créer des scènes 3D dans un navigateur web

* **babylon js** ([http://www.babylonjs.com/](http://www.babylonjs.com/))  
3D engine based on WebGL/Web Audio and JavaScript



## Modules de conversion

* **Pandoc** ([https://pandoc.org/](https://pandoc.org/))  
universal document converter, converting from one markup format to another, and a command-line tool that uses this library

* **Ink (Coko)** ([http://ink.coko.foundation/](http://ink.coko.foundation/))  
INK is a free, open source tool for constructing and managing file conversion pipelines : convert a document or its assets from one format to another, validate the syntax or content of a document, annotate or enhance a document...

* **GitBook** ([https://www.gitbook.com/](https://www.gitbook.com/))  
GitBook is a tool for building books using Git and Markdown. It can generate your book into: Static Website, pDF, eBook 

* **Asciidoctor** ([https://asciidoctor.org/](https://asciidoctor.org/))  
converting AsciiDoc content to HTML5, DocBook 5 (or 4.5) and other formats

.   
.   

## Module d'édition / entrée des données (écriture, corrections, login)

* **Substance** ([http://substance.io/](http://substance.io/))  
A JavaScript library provides building blocks for realizing custom text editors and web-based publishing systems.

* **XSweet**, Coko fundation ([https://gitlab.coko.foundation/XSweet/XSweet/wikis/home](https://gitlab.coko.foundation/XSweet/XSweet/wikis/home))  
A set of tools supporting data acquisition, editorial and document production workflows, on an XML stack with XML/HTML/CSS interfaces.


* **ContentBuilder.js** ([http://www.innovastudio.com/](https://gitlab.coko.foundation/XSweet/XSweet/wikis/home))  
InnovaStudio Content Builder (ContentBuilder.js) is a JQuery plugin that converts a DIV element into Editable Area. Unlike other editors, the plugin provides you with beautifully designed content blocks, ready to drag & drop. You can even create your own content blocks.

* **CKEditor 5** ([https://ckeditor.com/ckeditor-5-framework/](https://ckeditor.com/ckeditor-5-framework/))  
A set of components enabling you to create any kind of text editing solution.

* **Create js** ([http://createjs.org/](http://createjs.org/))    
web editing interface for Content Management Systems

* **TinyMCE** ([https://www.tinymce.com/](https://www.tinymce.com/))  

problématique : Presque rien n'est adapté au multimédia et à de l'*image first*


.   
## Modules Impression (HTML > PDF avec CSS)


3 types d'outils :

### 1. JavaScript tools in the browser

* **book.js** ([https://gist.github.com/MaiaVictor/b662b4d75561cb2acdeb21bf68421fc3](https://gist.github.com/MaiaVictor/b662b4d75561cb2acdeb21bf68421fc3))
* **CaSSius** ([https://github.com/MartinPaulEve/CaSSius](https://github.com/MartinPaulEve/CaSSius))

Ajoutent essentiellement des polyfill pour la fragmentation des contenus  
PDF généré par le boîte d'impression du navigateur  
plutôt utilisation expérimentale ou spécifique mais nécessitant beaucoup de développement javascript en addition 

problématique : aucune gestion des profil colorimétriques

### 2. Standalone tools

* **Prince** ([https://www.princexml.com/](https://www.princexml.com/)), actuellement le plus utilisé dans l'industrie
* **PDFreactor** ([http://www.pdfreactor.com/](http://www.pdfreactor.com/))
* **Antenna House** ([https://www.antennahouse.com/antenna1/](https://www.antennahouse.com/antenna1/))

outils propriétaires qui ont leur propre moteur de rendu  
utilisables en ligne de commande (parfois avec des interfaces pour simuler le PDF) ou implémentable dans les sites
utilisation professionnelles, les licences sont chères

problématique : peu de possibilités graphiques dans les layouts, besoin de regénérer le PDF à chaque modification dans le CSS, pas d'outils de développements
voir tableau de comparaison (en cours) pour savoir quelles features CSS sont supportées : [https://ethercalc.org/daqtlwmzmtq5](https://ethercalc.org/daqtlwmzmtq5)


### 3. Autres

* **Vivliostyle** ([https://vivliostyle.org/](https://vivliostyle.org/))  
tool used in the browser but is not a simple simulation of PDF as it offers a more complex display interface with the possibility to zoom or have the page in a spread / based on the EPUB Adaptive Layout specifications, impossible to add JavaScript

* **PagedMedia** ([http://www.pagedmedia.org/](http://www.pagedmedia.org/))  
en construction : "The project will develop a suite of Javascripts to paginate HTML/CSS in the browser, and to apply PagedMedia controls to paginated content for the purposes of exporting print-ready, or display-friendly, PDF from the browser.  This will be an Open Source initiative, appropriately licensed with the MIT license."

* **wkhtml** ([https://www.npmjs.com/package/wkhtmltopdf](https://www.npmjs.com/package/wkhtmltopdf))  
A Node.js wrapper for the wkhtmltopdf command line tool. As the name implies, it converts HTML documents to PDFs using WebKit  


.   
## Modules de génération pour l'epub

* **epub-gen** ([https://github.com/cyrilis/epub-gen](https://github.com/cyrilis/epub-gen) ) 
* **html-epub** de la Coko foundation ([https://gitlab.coko.foundation/pubsweet/html-epub](https://gitlab.coko.foundation/pubsweet/html-epub))  
* **epub maker** ([https://github.com/bbottema/js-epub-maker](https://github.com/bbottema/js-epub-maker) ) 
* **ebook maker** ([https://github.com/setanta/ebookmaker](https://github.com/setanta/ebookmaker) ) 
* **Calibre** ([https://calibre-ebook.com/about](https://calibre-ebook.com/about)  )
* **Sigil** ([https://sigil-ebook.com/about/](https://sigil-ebook.com/about/) - [https://github.com/Sigil-Ebook/Sigil](https://github.com/Sigil-Ebook/Sigil)  )

problématique : ces modules servent surtout à faire l'epub *reflowable* mais presque rien n'existe pour l'epub *fixed layout*, il faut discuter de cela avec l'apprimerie ou ABM

.   
## Modules bibliographiques

* **Bibtex** ([http://www.bibtex.org/](http://www.bibtex.org/))  
The word "BibTeX" stands for a tool and a file format which are used to describe and process lists of references, mostly in conjunction with LaTeX documents.

* **Zotero** ([https://www.zotero.org/](https://www.zotero.org/))  
Zotero is a free, easy-to-use tool to help you collect, organize, cite, and share research.

* **Mendeley** ([https://www.mendeley.com/](https://www.mendeley.com/))  

* **Endnote** ([http://carrefour.uquebec.ca/endnote/online/introduction](http://carrefour.uquebec.ca/endnote/online/introduction))  


## Modules annotation

* **Hypothesis** ([https://web.hypothes.is/](https://web.hypothes.is/))  
* **scrible** ([https://www.scrible.com/](https://www.scrible.com/))  
* **diigo** ([https://www.diigo.com/](https://www.diigo.com/))  
* **Pundit annotator** ([http://thepund.it/annotator-web-annotation/](http://thepund.it/annotator-web-annotation/))  
* **genius** ([https://genius.com/web-annotator](https://genius.com/web-annotator))  
* **Annotorious** ([https://annotorious.github.io/](https://annotorious.github.io/)) : annotation pour les images







